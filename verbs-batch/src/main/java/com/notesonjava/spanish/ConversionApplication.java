package com.notesonjava.spanish;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ConversionApplication {

	private static List<String> getWords(String original) {
		List<String> splitted = Arrays.asList(original.split("/"));
		return splitted.stream()
				.peek(System.out::println)
				.map(word -> Arrays.asList(word.split(",")))
				.peek(System.out::println)
				.flatMap(List::stream)
				.map(current -> current.trim())
				.map(current -> {
					if(current.startsWith("to ")) {
						return current.substring(3);
					}
					return current;
				})
				.peek(System.out::println)
				.collect(Collectors.toList());
		
	}
	
	public static void main(String[] args) throws IOException {
		Path original = Paths.get("./verbs.json");
		ObjectMapper mapper = new ObjectMapper();
		String json = new String(Files.readAllBytes(original));
		List<OriginalVerb> list = Arrays.asList(mapper.readValue(json, OriginalVerb[].class));
		
		List<JsonVerb> converted = list.stream().map(current -> {
			JsonVerb verb = new JsonVerb();
			verb.setRegular(current.isRegular());
			verb.setViews(current.getViews());
			verb.setSpanish(current.getSpanish());
			verb.addTranslations(getWords(current.getEnglish()) , Language.ENGLISH);
			if(current.getFrench() != null) {
				verb.addTranslations(getWords(current.getFrench()), Language.FRENCH);
			}
			return verb;
		}).peek(System.out::println)
		.collect(Collectors.toList());
		File result = new File("converted.json");
		
		
		if(result.exists()) {
			result.delete();
		}
		
		mapper.writerWithDefaultPrettyPrinter().writeValue(result, converted);
	}
}

/*
 * 
 * Document doc = new Document();
			doc.put("amount", tx.getAmount());
			doc.put("price", tx.getPrice());
			doc.put("buy_order_id", tx.getBuyOrderId());
			doc.put("sell_order_id", tx.getSellOrderId());
			doc.put("server_id", tx.getServerId());
			doc.put("currency_pair", tx.getCurrencyPair());
			doc.put("origin", tx.getOrigin());
			doc.put("server_time", tx.getServerTime());
			doc.put("update_time", tx.getUpdateTime());
 */
