package com.notesonjava.spanish;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Data;

@Data
public class JsonVerb {
		
	private String spanish;
	private Map<Language, Set<String>> translations = new HashMap<>();
	private boolean regular;
	
	private int views;
	
	
	public Set<String> getTranslations(Language language){
		return Collections.unmodifiableSet(translations.get(language));
	}

	public void addTranslation(String word, Language language) {
		if(translations.containsKey(language)){
			translations.get(language).add(word);
		} else {
			Set<String> set = new HashSet<>();
			set.add(word);
			translations.put(language, set);
		}
 	}
	
	public void addTranslations(Collection<String> words, Language language) {
		if(translations.containsKey(language)){
			translations.get(language).addAll(words);
		} else {
			translations.put(language, new HashSet<>(words));
		}
		
	}
}
