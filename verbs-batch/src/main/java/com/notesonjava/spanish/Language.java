package com.notesonjava.spanish;

public enum Language {

	ENGLISH(0,"english"),
	FRENCH(1,"french"),
	NONE(999, "none");
	
	private int id;
	private String language;
	private Language(int id,String language){
		this.id = id;
		this.language = language;
	}
	
	public String getLanguage(){
		return language;
	}
	
	public int getId(){
		return id;
	}
	
	public static Language fromValue(int value){
		Language[] values = Language.values();
		
		for(int i=0; i< values.length; i++){
			if(value == values[i].id){
				return values[i];
			}
		}
		return Language.NONE;
		
	}
	
	public static Language fromValue(String value){
		Language[] values = Language.values();
		
		for(int i=0; i< values.length; i++){
			if(value.toLowerCase().equals(values[i].language)){
				return values[i];
			}
		}
		return Language.NONE;
		
	}
}
