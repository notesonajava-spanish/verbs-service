package com.notesonjava.spanish;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bson.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.compose.DockerComposeCommands;
import com.notesonjava.commands.mongo.MongoToolsCommands;

public class ConversionApplicationDump {

	private static List<String> getWords(String original) {
		List<String> splitted = Arrays.asList(original.split("/"));
		return splitted.stream()
				.peek(System.out::println)
				.map(word -> Arrays.asList(word.split(",")))
				.peek(System.out::println)
				.flatMap(List::stream)
				.map(current -> current.trim())
				.map(current -> {
					if(current.startsWith("to ")) {
						return current.substring(3);
					}
					return current;
				})
				.peek(System.out::println)
				.collect(Collectors.toList());
	}

	
	public static void main(String[] args) throws IOException {
		Path original = Paths.get("./verbs.json");
		
		int port = 27018;
		String host = "localhost";
		String dbName = "verbs";
		String archiveName = "verbs.gz";
		
		ObjectMapper mapper = new ObjectMapper();
		String json = new String(Files.readAllBytes(original));
		List<OriginalVerb> list = Arrays.asList(mapper.readValue(json, OriginalVerb[].class));
		
		CommandResult composeResult = DockerComposeCommands.up();	
		composeResult.getOutput().forEach(System.out::println);
		
		MongoClient client = new MongoClient(host, port);
		MongoDatabase db = client.getDatabase(dbName);
	
		insertVerbs(list, db);
		CommandResult result = MongoToolsCommands.archiveDump(archiveName, host, port);
		result.getOutput().forEach(System.out::println);
		
		client.close();
		
		CommandResult closeResult = DockerComposeCommands.down();	
		closeResult.getOutput().forEach(System.out::println);
		
	}

	private static void insertVerbs(List<OriginalVerb> list, MongoDatabase db) {
		MongoCollection<Document> collection = db.getCollection("verbs");
		
		
		List<Document> converted = list.stream().map(current -> {
			Document doc = new Document();
			doc.put("regular", current.isRegular());
			doc.put("views", current.getViews());
			doc.put("spanish",current.getSpanish());
			Document translations = new Document();
			translations.put(Language.ENGLISH.toString(), getWords(current.getEnglish()));
			if(current.getFrench() != null) {
				translations.put(Language.FRENCH.toString(),getWords(current.getFrench()));
			}
			doc.put("translations", translations);
			return doc;
		})
		.collect(Collectors.toList());
		
		collection.insertMany(converted);
	}
}


