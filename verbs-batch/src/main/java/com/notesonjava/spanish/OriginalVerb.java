package com.notesonjava.spanish;

import lombok.Data;

@Data
public class OriginalVerb {

	private int views;
	private String spanish;
	private String french;
	private String english;
	private boolean regular;
}
