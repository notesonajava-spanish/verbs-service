package com.notesonjava.spanish.integration;


import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.VerbApplication;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;
import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbWithSynonyms;
import com.notesonjava.spanish.repos.VerbRepository;
import com.notesonjava.spanish.repos.VerbService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
@Tag("db")
public class VerbServiceIntg {
	
	private static MongoClient mongoClient;
	private static VerbService service;
	

	@BeforeAll
	public static void init() throws IOException, InterruptedException  {
		
		System.out.println("Before VerbServiceTest");
		PropertyMap props = PropertyLoader.loadProperties();
		
		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
		mongoClient = VerbApplication.createLocalClient(dbHost, dbPort);
		
		MongoDatabase database = mongoClient.getDatabase("verbs");
		DocumentAdapter adapter = new DocumentAdapter();
		
		VerbRepository repo = new VerbRepository(database, adapter);
		service = new VerbService(repo);
		
	}
	
	@AfterAll
	public static void close() {
		mongoClient.close();
	
		System.out.println("Finish VerbServiceTest");
		
	}
	
	
	@Test
	public void test_synonyms() {
		VerbWithSynonyms synonyms = service.getVerbWithSynonym("hacer", Language.ENGLISH);
		Assertions.assertThat(synonyms.getSynonyms()).hasSize(1);
		Assertions.assertThat(synonyms.getSynonyms().stream().map(v -> v.getSpanish()).collect(toList())).doesNotContain("hacer");
		
	}
	
	@Test
	public void test_suffix_venir() {
		List<Verb> verbs =service.getBySuffix(Arrays.asList("venir"));
		Assertions.assertThat(verbs).hasSize(5);
		verbs.forEach(v -> {
			Assertions.assertThat(v.getSpanish()).endsWith("venir");
		});
	}
	
	@Test
	public void test_multi_suffix() {
		List<Verb> verbs =service.getBySuffix(Arrays.asList("venir", "tener"));
		Assertions.assertThat(verbs).hasSize(13);
		verbs.forEach(v -> {
			boolean valid = v.getSpanish().endsWith("venir") || v.getSpanish().endsWith("tener"); 
			Assertions.assertThat(valid).isTrue();
		});
	}
}
