package com.notesonjava.spanish.integration;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.VerbApplication;
import com.notesonjava.spanish.extensions.DockerComposeExtension;
import com.notesonjava.spanish.extensions.MongoRestoreExtension;
import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.repos.VerbRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(DockerComposeExtension.class)
@ExtendWith(MongoRestoreExtension.class)
@Tag("intg")
@Tag("db")
public class VerbRepositoryIntg {

	private static VerbRepository repo;
	
	private static MongoClient mongoClient;
	
	
	@BeforeAll
	public static void init() throws IOException {
		log.debug("BeforeClass VerbRepositoryTest");
		
		
		PropertyMap props = PropertyLoader.loadProperties();
		
		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
		
		mongoClient = VerbApplication.createLocalClient(dbHost, dbPort);
		
		MongoDatabase database = mongoClient.getDatabase("verbs");
		DocumentAdapter adapter = new DocumentAdapter();
		
		
		
		repo = new VerbRepository(database, adapter);
	
	}
	
	@AfterAll
	public static void close() {
		
		mongoClient.close();
	
		log.info("AfterClass VerbRepositoryTest");
		
	}


	@Test
	public void test_find_all(){
		List<Verb> verbs = repo.findAll();
		
		assertThat(verbs).hasSize(1220);
		
	}
	
	@Test
	public void test_find_regular_ar(){
		List<Verb> verbs = repo.findRegularsEndingBy("ar");
		assertThat(verbs).hasSize(597);
		for(Verb v : verbs){
			assertThat(v.getSpanish()).endsWith("ar");
			assertThat(v.isRegular()).isTrue();
		}
	}
	
	@Test
	public void test_find_regular_ir(){
		List<Verb> verbs = repo.findRegularsEndingBy("ir");
		assertThat(verbs).hasSize(62);
		for(Verb v : verbs){
			assertThat(v.getSpanish()).endsWith("ir");
			assertThat(v.isRegular()).isTrue();
		}
	}
	
	@Test
	public void test_find_regular_er(){
		List<Verb> verbs = repo.findRegularsEndingBy("er");
		assertThat(verbs).hasSize(40);
		for(Verb v : verbs){
			assertThat(v.getSpanish()).endsWith("er");
			assertThat(v.isRegular()).isTrue();
		}
	}
	
	@Test
	public void test_find_regular_order_by_views(){
		List<Verb> verbs = repo.findRegularsEndingWithOrderByViewsDesc("er");
		assertThat(verbs).hasSize(40);
		int previous = Integer.MAX_VALUE;
		for(Verb v : verbs){
			assertThat(v.getSpanish()).endsWith("er");
			assertThat(v.isRegular()).isTrue();
			assertThat(v.getViews()).isLessThanOrEqualTo(previous);
			previous = v.getViews();
			
		}
	}
	
	@Test
	public void test_find_er(){
		List<Verb> verbs = repo.findEndingWith("er");
		Assertions.assertThat(verbs).isNotEmpty();
		for(Verb v : verbs){
			assertThat(v.getSpanish()).endsWith("er");
			
		}
	}

	
	@Test
	public void test_find_top_200(){
		List<Verb> verbs = repo.findByOrderByViewsDesc();
		int previous = Integer.MAX_VALUE;
		for(Verb v : verbs){
			assertThat(v.getViews()).isLessThanOrEqualTo(previous);
			previous = v.getViews();
		}
	}
	
	@Test
	public void test_find_pages(){
		List<Verb> verbs = repo.findByOrderByViewsDesc();
		int previous = Integer.MAX_VALUE;
		for(Verb v : verbs){
			assertThat(v.getViews()).isLessThanOrEqualTo(previous);
			previous = v.getViews();
			
		}
	}
	
	@Test
	public void test_find_regular(){
		List<Verb> verbs = repo.findRegulars();
		Assertions.assertThat(verbs).isNotEmpty();
		for(Verb v : verbs){
			assertThat(v.isRegular()).isTrue();
		}
	}
	
	@Test
	public void test_find_irregular(){
		List<Verb> verbs = repo.findIrregulars();
		for(Verb v : verbs){
			assertThat(v.isRegular()).isFalse();
		}
	}
	
	
	@Test
	public void test_english_synonyms_of_have(){
		Verb haber = repo.findBySpanish("haber").get();
		Verb tener = repo.findBySpanish("tener").get();
		List<Verb> list = repo.findByWordAndLanguage("have",Language.ENGLISH);
		assertThat(list).hasSize(2);
		assertThat(list).contains(haber, tener);
	}
	
	@Test
	public void test_find_by_spanish_in(){
		Verb haber = repo.findBySpanish("haber").get();
		Verb tener = repo.findBySpanish("tener").get();
		
		List<Verb> list = repo.findBySpanishIn(Arrays.asList("haber", "tener"));
		assertThat(list).hasSize(2);
		assertThat(list).contains(haber, tener);
	}
	
	@Test
	public void test_find_multi_verbs(){
		Verb haber = repo.findBySpanish("haber").get();
		Verb tener = repo.findBySpanish("tener").get();
		Verb tocar = repo.findBySpanish("tocar").get();
		
		
		
		List<Verb> list = repo.findByWordsAndLanguage(new HashSet<>(Arrays.asList("have", "touch")), Language.ENGLISH);
		assertThat(list).hasSize(3);
		assertThat(list).contains(haber, tener, tocar);
	}
	
	
	@Test
	public void test_find_translation_existing(){
		List<Verb> list = repo.findByWordAndLanguage("have", Language.ENGLISH);
		Assertions.assertThat(list).isNotEmpty();
		Assertions.assertThat(list).hasSize(2);
		
	}

	@Test
	public void test_find_ending_by_venir() {
		List<Verb> list = repo.findEndingWith("venir");
		Assertions.assertThat(list).isNotEmpty();
		Assertions.assertThat(list).hasSize(5);
		
	}
}
