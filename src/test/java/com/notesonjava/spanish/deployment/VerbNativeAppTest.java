package com.notesonjava.spanish.deployment;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.assertj.core.api.Assertions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.VerbApplication;
import com.notesonjava.spanish.VerbController;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbWithSynonyms;
import com.notesonjava.spanish.repos.VerbRepository;
import com.notesonjava.spanish.repos.VerbService;

import io.javalin.Javalin;
import io.javalin.core.util.RouteOverviewPlugin;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Tag("deployment")
public class VerbNativeAppTest {

	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";

	private static String url = "http://localhost:8080";
	private ObjectMapper mapper = new ObjectMapper();

	private static MongoClient mongoClient;

	private static Javalin app;
	
	
	@BeforeClass
	public static void init() throws IOException, InterruptedException {

		log.info("BeforeClass VerbApplicationIntegration");
		
		PropertyMap props = PropertyLoader.loadProperties();

		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		int port = Integer.parseInt(props.get("server.port").orElse("8080"));

		
		mongoClient = VerbApplication.createLocalClient(dbHost, dbPort);

		MongoDatabase database = mongoClient.getDatabase("verbs");

		DocumentAdapter adapter = new DocumentAdapter();
		VerbRepository verbRepo = new VerbRepository(database, adapter);

		VerbService service = new VerbService(verbRepo);
		app = Javalin.create(config -> {
			config.registerPlugin(new RouteOverviewPlugin("/"));
			config.enableCorsForAllOrigins();
			config.defaultContentType = APPLICATION_JSON_UTF8;
		});
			
		VerbController controller = new VerbController(app, verbRepo, service);
		controller.setup();
		app.start(port);

		url = "http://localhost:" + port;

	}

	@AfterClass
	public static void clean() {
		mongoClient.close();
		app.stop();
	}

	@Test
	public void test_find_synonyms_hacer() throws JsonParseException, JsonMappingException, IOException {
		HttpResponse response = HttpRequest.get(url + "/synonyms/hacer").send();
		Assertions.assertThat(response.statusCode()).isEqualTo(200);
		Assertions.assertThat(response.contentType()).isEqualTo(APPLICATION_JSON_UTF8);

		validateCors(response);

		VerbWithSynonyms body = mapper.readValue(response.bodyText(), VerbWithSynonyms.class);
		Assertions.assertThat(body.getVerb().getSpanish()).isEqualTo("hacer");
		Assertions.assertThat(body.getSynonyms().size()).isEqualTo(1);
		Assertions.assertThat(body.getSynonyms().get(0).getSpanish()).isEqualTo("fabricar");
	}



	@Test
	public void test_popular() throws JsonParseException, JsonMappingException, IOException {
		log.info("Call popular verbs");
		HttpResponse response = HttpRequest.get(url + "/popular").send();

		validateCors(response);

		Assertions.assertThat(response.statusCode()).isEqualTo(200);
		Assertions.assertThat(response.contentType()).isEqualTo(APPLICATION_JSON_UTF8);
		Verb[] body = mapper.readValue(response.bodyText(), Verb[].class);
		Assertions.assertThat(body.length).isEqualTo(200);

	}

	@Test
	public void test_find_synonyms_tener() throws JsonParseException, JsonMappingException, IOException {
		HttpResponse response = HttpRequest.get(url + "/synonyms/tener").send();

		validateCors(response);

		VerbWithSynonyms synonyms = mapper.readValue(response.bodyText(), VerbWithSynonyms.class);
		Assertions.assertThat(synonyms.getSynonyms()).hasSize(1);
	}

	@Test
	public void test_find_synonyms_enjoy_disfrutar() throws JsonParseException, JsonMappingException, IOException {
		HttpResponse response = HttpRequest.get(url + "/synonyms/disfrutar").send();

		validateCors(response);

		VerbWithSynonyms synonyms = mapper.readValue(response.bodyText(), VerbWithSynonyms.class);

		Assertions.assertThat(synonyms.getSynonyms()).hasSize(1);
	}

	@Test
	public void test_find_verbs_ending_with_venir() throws JsonParseException, JsonMappingException, IOException {
		HttpResponse response = HttpRequest.get(url + "/suffix?suffix=venir&suffix=tener").send();

		validateCors(response);

		Verb[] verbs = mapper.readValue(response.bodyText(), Verb[].class);

		Assertions.assertThat(verbs).hasSize(13);
		Arrays.asList(verbs).forEach(v -> {
			String spanish = v.getSpanish();
			Assertions.assertThat(spanish.endsWith("tener") || spanish.endsWith("venir")).isTrue();
		});
	}

	@Test
	public void test_find_synonyms_comenzar_iniciar_empezar() {

		List<String> verbs = Arrays.asList("comenzar", "iniciar", "empezar");

		verbs.forEach(verb -> {
			HttpResponse response = HttpRequest.get(url + "/synonyms/" + verb).send();

			validateCors(response);

			try {
				VerbWithSynonyms result = mapper.readValue(response.bodyText(), VerbWithSynonyms.class);
				List<String> synonyms = result.getSynonyms().stream().map(v -> v.getSpanish())
						.collect(Collectors.toList());
				List<String> otherVerbs = verbs.stream().filter(v -> !v.equals(verb)).collect(Collectors.toList());
				Assertions.assertThat(synonyms).hasSize(otherVerbs.size());
				Assertions.assertThat(synonyms).containsAll(otherVerbs);
				Assertions.assertThat(synonyms).doesNotContain(verb);
			} catch (Exception e) {
				e.printStackTrace();
			}

		});

	}

	@Test
	public void validate_ensenar_have_accent() throws JsonParseException, JsonMappingException, IOException {

		HttpResponse response = HttpRequest.get(url + "/popular?end=200").send();

		validateCors(response);
	
		Verb[] result = mapper.readValue(response.bodyBytes(), Verb[].class);
		List<Verb> list = Arrays.asList(result);
		boolean match = list.stream().anyMatch(v -> v.getSpanish().equals("enseñar"));
		
		Assertions.assertThat(match).isTrue();

	}
	
	@Test
	public void validate_ending_with_nar() throws JsonParseException, JsonMappingException, IOException {

		HttpResponse response = HttpRequest.get(url + "/suffix?suffix=ñar").send();
		
		Verb[] result = mapper.readValue(response.bodyBytes(), Verb[].class);
		List<Verb> list = Arrays.asList(result);
		boolean match = list.stream().anyMatch(v -> v.getSpanish().equals("enseñar"));
		
		Assertions.assertThat(match).isTrue();

	}
	
	@Test
	public void validate_no_special_character() throws JsonParseException, JsonMappingException, IOException {

		HttpResponse response = HttpRequest.get(url + "/popular").send();

		validateCors(response);

			Verb[] result = mapper.readValue(response.bodyBytes(), Verb[].class);
			
			List<Verb> list = Arrays.asList(result);
			Assertions.assertThat(list.stream().noneMatch(v -> v.getSpanish().contains("±"))).isTrue();
			Assertions.assertThat(list.stream().noneMatch(v -> v.getSpanish().contains("&"))).isTrue();
			Assertions.assertThat(list.stream().noneMatch(v -> v.getSpanish().contains("Ã"))).isTrue();
			
	}
	
	
	private void validateCors(HttpResponse response) {
		log.info("Response: " + response);
		Assertions.assertThat(response.header("Access-Control-Allow-Methods")).isEqualTo("GET,OPTIONS");
		Assertions.assertThat(response.header("Access-Control-Allow-Headers"))
				.isEqualTo("X-Requested-With,Accept,Origin");
		Assertions.assertThat(response.header("Access-Control-Request-Method")).isEqualTo("GET,OPTIONS");
		Assertions.assertThat(response.header("Access-Control-Allow-Credentials")).isEqualTo("false");
	}
}
