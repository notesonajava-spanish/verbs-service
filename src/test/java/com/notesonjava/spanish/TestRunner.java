package com.notesonjava.spanish;

import java.io.IOException;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;
import static org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder.request;


import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherConfig;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestRunner {

	public static void main(String[] args) throws IOException {

		LauncherConfig config = LauncherConfig.builder()
				.addTestExecutionListeners(new SummaryGeneratingListener())
				.build();

		
		LauncherDiscoveryRequest request = request()
			    .selectors(selectPackage("com.notesonjava.spanish.deployment"))
			    .build();

		
		Launcher launcher = LauncherFactory.create(config);

		SummaryGeneratingListener listener = new SummaryGeneratingListener();
		launcher.registerTestExecutionListeners(listener);
		launcher.execute(request);
		TestExecutionSummary summary = listener.getSummary();
		log.info(summary.toString());
		log.info("Failures: " + summary.getTotalFailureCount());
		log.info("Containers Succeed: " + summary.getContainersSucceededCount());
		log.info("Tests succeeded: " + summary.getTestsSucceededCount());
		
		log.info("Failures: ");
		summary.getFailures().forEach(f -> {
			log.info(f.getTestIdentifier().getDisplayName());
			log.error("Failure: ", f.getException());
		});
		
		
		System.exit((int)summary.getTestsFailedCount());
				
	}
}
