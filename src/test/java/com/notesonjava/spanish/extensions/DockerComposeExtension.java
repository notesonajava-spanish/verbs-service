package com.notesonjava.spanish.extensions;

import java.util.Optional;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.compose.DockerComposeCommands;
import com.notesonjava.commands.compose.DockerInitializationException;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DockerComposeExtension implements BeforeAllCallback, AfterAllCallback {

	private static boolean useCompose;

	@NonNull
	private String[] composeFiles = { "docker-compose-test.yml" };


	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		log.info("Shutting down compose network");
		
		if (useCompose) {
			DockerComposeCommands.down(composeFiles);
		}
		log.info("Compose network should be shutdown");
		
	}

	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		log.info("Starting compose network");
		PropertyMap props = PropertyLoader.loadProperties();
		useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		
		if (useCompose) {
			CommandResult buildResult = DockerComposeCommands.build(composeFiles);
			if (buildResult != null) {
				buildResult.getOutput().forEach(s -> log.info(s));
			}
			CommandResult composeResult = DockerComposeCommands.up(composeFiles);
			if (composeResult != null) {
				composeResult.getOutput().forEach(s -> log.info(s));
				Optional<String> error = composeResult.getOutput().stream().filter(l -> l.contains("ERROR")).findFirst();
				if(error.isPresent()) {
					throw new DockerInitializationException(error.get());
				}
			} else {
				throw new DockerInitializationException("DockerCompose didn't answer in time");
			}
				
		}
		
		log.info("Compose network should be started");
	}

	
}

