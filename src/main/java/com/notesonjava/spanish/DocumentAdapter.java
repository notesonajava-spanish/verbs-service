package com.notesonjava.spanish;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DocumentAdapter {

	public Verb convert(Document document) {
	
		log.debug("Convert doc : " + document.toJson());
		
		Verb verb = new Verb();
		verb.setRegular(document.getBoolean("regular"));
		verb.setSpanish(document.getString("spanish"));
		
		/* 
		    20/11/2018 
		    The restore is not consistent, possibly a conflict between mongo tools inside a docker image and runtime environment.
		    mongorestore from the same dump in json or gzip create the views column in double or integer.
		    Calling document.getInteger() or document.getDouble() cause a ClassCastException depending on the type created by the restore.
		*/
		Object viewObject = document.get("views");
		if(viewObject instanceof Double) {
			Double views = (Double) viewObject;
			verb.setViews((int)Math.round(views));
		}
		if(viewObject instanceof Integer) {
			Integer views = (Integer) viewObject;
			verb.setViews(views);
		}
		
		
		Object obj = document.get("translations");
		if(obj != null && obj instanceof Document) {
			Document translations = (Document)obj;
		
			ArrayList<String> english = translations.get(Language.ENGLISH.toString(), ArrayList.class);
			verb.addTranslations(english, Language.ENGLISH);
				
			ArrayList<String> french = translations.get(Language.FRENCH.toString(), ArrayList.class);
			if(french!=null) {
				verb.addTranslations(french, Language.FRENCH);
			}
			
		}
		
		log.debug("Converted : " + verb);
		
		return verb;
	}


	public List<Verb> convertResult(Iterator<Document> iter) {
		List<Verb> result = new ArrayList<>();
		
		while(iter.hasNext()) {
			Document document = iter.next();
			String json = document.toJson();
			log.debug("Json doc : " + json);
			Verb verb = convert(document);
			result.add(verb);
			
		}
		log.debug("Result size : " + result.size());
		return result;
	}
	
	
}
