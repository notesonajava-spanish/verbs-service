package com.notesonjava.spanish.subsititutions;

import java.util.Arrays;
import java.util.List;

import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;


@TargetClass(className = "com.mongodb.internal.dns.DnsResolver")
final class Target_com_mongodb_internal_dns_DnsResolver {
	
	@Substitute
	public static List<String> resolveHostFromSrvRecords(final String srvHost) {
		System.out.println("Inside Substitution");
		return Arrays.asList("cluster0-shard-00-01-skws2.mongodb.net:27017", "cluster0-shard-00-00-skws2.mongodb.net:27017", "cluster0-shard-00-02-skws2.mongodb.net:27017");
    }
	
	@Substitute
	public static String resolveAdditionalQueryParametersFromTxtRecords(final String host) {
		return "";
	}
}
	
public class MongoSubstitution {

}
