package com.notesonjava.spanish;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CorsHandler implements Handler {
	
	@Override
	public void handle(Context ctx) throws Exception {
		String origin = ctx.header("Origin");
		if(VerbController.authorizedUrl.contains(origin)) {
			ctx.header("Access-Control-Allow-Origin", origin);	
			
		}
		
		ctx.header("Access-Control-Allow-Methods", "GET,OPTIONS");
		ctx.header("Access-Control-Allow-Headers", "X-Requested-With,Accept,Origin");
		ctx.header("Access-Control-Request-Method", "GET,OPTIONS");
		ctx.header("Access-Control-Allow-Credentials", "false");
		
		if(log.isDebugEnabled()) {
			log.debug("*** Response header");
			ctx.headerMap().entrySet().forEach(entry ->  log.debug(entry.getKey() +", "+ entry.getValue()));
			log.debug("*** ");
		}
	}
}