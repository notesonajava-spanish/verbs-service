package com.notesonjava.spanish.repos;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbWithSynonyms;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class VerbService {
	
	private VerbRepository repo;
	
	
	public List<Verb> findSynonyms(Verb verb, Language language){
		List<Verb> matches = repo.findByWordsAndLanguage(verb.getTranslations(language), language);
		return matches.stream()
				.filter(v -> !v.equals(verb))
				.collect(toList());
	}
	
	public List<Verb> getBySuffix(List<String> suffixes){
		return suffixes.stream().map(s -> repo.findEndingWith(s)).flatMap(List::stream).collect(toList());
		
	}


	public VerbWithSynonyms getVerbWithSynonym(String spanish, Language language){
		
		VerbWithSynonyms result = new VerbWithSynonyms();
		Optional<Verb> verb = repo.findBySpanish(spanish);
		log.debug("Found verb : " + spanish);
		if(verb.isPresent()){
			log.debug("Found verb : " + verb.get());
			Verb v = verb.get();
			List<Verb> synonyms = findSynonyms(v, language);
			result.setVerb(verb.get());
			result.addSynonyms(synonyms);	
		} 
		return result;
	}
	
	public List<VerbWithSynonyms> search(List<String> spanish, Language language){
		return spanish.stream().map(s -> getVerbWithSynonym(s, language)).collect(Collectors.toList());
		
	}
	
	
}
