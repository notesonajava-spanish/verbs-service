package com.notesonjava.spanish.repos;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.regex;
import static com.mongodb.client.model.Sorts.descending;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class VerbRepository {


	private static final String COLLECTION_NAME = "verbs";

	private MongoDatabase database;
	
	private DocumentAdapter adapter;

	
	
	public List<Verb> findAll() {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection.find();
		return adapter.convertResult(findIter.iterator());
	
	}
	
	public List<Verb> findByWordAndLanguage(String word, Language language){
		log.debug("Search by word : "+ word +" - "+language);
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		String key = "translations.ENGLISH";
		
		FindIterable<Document> findIter = collection.
				find(and(in(key, word)));
		
		List<Verb> result = adapter.convertResult(findIter.iterator());
		return result;
	}
	
	public List<Verb> findByWordsAndLanguage(Set<String> words, Language language){
		
		return words.stream().map(w -> findByWordAndLanguage(w, language)).flatMap(List::stream).distinct().collect(Collectors.toList());
	}
	

	public List<Verb> findRegularsEndingWithOrderByViewsDesc(String ending) {
		//@formatter:off
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection
				.find(and(eq("regular", true), regex("spanish", ending + "$")))
				.sort(descending("views"));
		return adapter.convertResult(findIter.iterator());
		
		//@formatter:on
	}

	public List<Verb> findRegularsEndingBy(String ending) {
		//@formatter:off
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection
					.find(and(eq("regular", true), regex("spanish", ending + "$")));
		return adapter.convertResult(findIter.iterator());
		//@formatter:on
	}

	public List<Verb> findEndingWith(String ending) {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		
		FindIterable<Document> findIter = collection.find(regex("spanish", ending+"$"));
		
		return adapter.convertResult(findIter.iterator());
	}

	
	public Optional<Verb> findBySpanish(String spanish) {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		
		FindIterable<Document> findIter = collection.find(eq("spanish", spanish));
		List<Verb> result = adapter.convertResult(findIter.iterator());
		
		if(result.isEmpty()) {
			return Optional.empty();
		}
		
		return Optional.of(result.get(0));
	}

	public List<Verb> findBySpanishIn(List<String> spanish) {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection.find(in("spanish", spanish));
		return adapter.convertResult(findIter.iterator());
	}

	public List<Verb> findByTranslation(String word, Language language) {
		
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);

		FindIterable<Document> findIter = collection.find();
		List<Verb> verbs = adapter.convertResult(findIter.iterator());
		return verbs.stream().filter(v -> v.getTranslations(language).contains(word)).collect(Collectors.toList());
	}

	public List<Verb> findRegulars() {
		
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection.find(eq("regular", true));
		return adapter.convertResult(findIter.iterator());
	}

	public List<Verb> findIrregulars() {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection.find(eq("regular", false));
		return adapter.convertResult(findIter.iterator());
	}

	public List<Verb> findByOrderByViewsDesc() {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		FindIterable<Document> findIter = collection.find().sort(descending("views"));
		return adapter.convertResult(findIter.iterator());
	}

}