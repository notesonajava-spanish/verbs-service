package com.notesonjava.spanish.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.Data;

@Data
public class Verb {
		
	private String spanish;
	private Map<String, Set<String>> translations = new HashMap<>();
	private boolean regular;
	
	private int views;
	
	
	public Set<String> getTranslations(Language language){
		return Collections.unmodifiableSet(translations.get(language.getCode()));
	}

	public void addTranslation(String word, Language language) {
		if(translations.containsKey(language.getCode())){
			translations.get(language.getCode()).add(word);
		} else {
			Set<String> set = new HashSet<>();
			set.add(word);
			translations.put(language.getCode(), set);
		}
 	}
	
	public void addTranslations(Collection<String> words, Language language) {
		if(translations.containsKey(language.getCode())){
			translations.get(language.getCode()).addAll(words);
		} else {
			translations.put(language.getCode(), new HashSet<>(words));
		}
		
	}
}
