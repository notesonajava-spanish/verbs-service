package com.notesonjava.spanish.model;

public enum Language {

	ENGLISH(0,"en"),
	FRENCH(1,"fr"),
	NONE(999, "none");
	
	private int id;
	private String code;
	private Language(int id,String code){
		this.id = id;
		this.code = code;
	}
	
	public String getCode(){
		return code;
	}
	
	public int getId(){
		return id;
	}
	
	public static Language fromValue(int value){
		Language[] values = Language.values();
		
		for(int i=0; i< values.length; i++){
			if(value == values[i].id){
				return values[i];
			}
		}
		return Language.NONE;
		
	}
	
	public static Language fromValue(String value){
		Language[] values = Language.values();
		
		for(int i=0; i< values.length; i++){
			if(value.toLowerCase().equals(values[i].code)){
				return values[i];
			}
		}
		return Language.NONE;
		
	}
}
