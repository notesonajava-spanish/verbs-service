package com.notesonjava.spanish.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VerbWithSynonyms {

	private Verb verb;
	private List<Verb> synonyms = new ArrayList<>();
	
	public void addSynonym(Verb v){
		synonyms.add(v);
	}
	
	public void addSynonyms(List<Verb> list){
		synonyms.addAll(list);
	}
}
