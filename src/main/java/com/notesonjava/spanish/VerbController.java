package com.notesonjava.spanish;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.model.Language;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbWithSynonyms;
import com.notesonjava.spanish.repos.VerbRepository;
import com.notesonjava.spanish.repos.VerbService;

import io.javalin.Javalin;
import io.javalin.http.Handler;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbController {
	
	static final List<String> authorizedUrl = Arrays.asList( "http://localhost:4200" );
	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";
	
	private Javalin app;
	private VerbRepository verbRepo;
	
	private VerbService service;
	
	
	public void setup() {
//		Handler preflightHandler = new PreflightHandler();
		Handler corsHandler = new CorsHandler();
		
		ObjectMapper mapper = new ObjectMapper();
		
		
		app.options("/popular", ctx -> ctx.status(200));
		
		app.get("/popular", ctx -> {
			ctx.status(200);
			ctx.contentType(APPLICATION_JSON_UTF8);
			
			Integer end = 200;
			
			if(ctx.queryParam("end") != null){
				String limit = ctx.queryParam("end");
				log.debug("Limit : " + limit);
				end = Integer.parseInt(limit);
			}
			log.debug("Search " + end + " verbs");
			List<Verb> result = verbRepo.findByOrderByViewsDesc().stream().limit(end).collect(Collectors.toList());
			
			String json = mapper.writeValueAsString(result);
			ctx.result(json);
			
		});
		
		app.options("/synonyms/:spanish", ctx -> ctx.status(200));
		
		app.get("/synonyms/:spanish", ctx -> {
			ctx.status(200);
			ctx.contentType(APPLICATION_JSON_UTF8);
			
			String spanish = ctx.pathParam(":spanish");
			log.debug("Search spanish for : " +spanish);
			VerbWithSynonyms result = service.getVerbWithSynonym(spanish, Language.ENGLISH);
		
			ctx.result(mapper.writeValueAsString(result));			
		});

		app.options("/synonyms", ctx -> ctx.status(200));
		app.get("/synonyms", ctx -> {
			ctx.status(200);
			ctx.contentType(APPLICATION_JSON_UTF8);
			
			List<String> spanish = ctx.queryParams("verb");
			
			String json = mapper.writeValueAsString(service.search(spanish, Language.ENGLISH));
			ctx.result(json);
		});
	
		app.options("/suffix", ctx-> ctx.status(200));
		
		app.get("/suffix", ctx -> {
			ctx.status(200);
			
			log.debug("Query string : " + ctx.queryString());
			List<String> suffixes = ctx.queryParams("suffix");
			List<Verb> result = service.getBySuffix(suffixes);
			log.debug("Found by suffix : " + result);
			String json = mapper.writeValueAsString(result);
			log.debug(json);
			ctx.result(json);	
		});
		app.after(corsHandler);
	}

}
