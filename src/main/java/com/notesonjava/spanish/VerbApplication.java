package com.notesonjava.spanish;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.repos.VerbRepository;
import com.notesonjava.spanish.repos.VerbService;

import io.javalin.Javalin;
import io.javalin.core.util.RouteOverviewPlugin;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class VerbApplication {
	
	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";


	public static MongoClient createRemoteClient(String user, String password, String host, String db) {
		MongoClientURI uri = new MongoClientURI("mongodb+srv://"+user+":"+password+"@"+host+"/"+db);
		return new MongoClient(uri);
	}
	
	public static MongoClient createLocalClient(String host, int port) {
		MongoClientURI uri = new MongoClientURI("mongodb://"+host+":"+port);
		
		return new MongoClient(uri);
	}
	
	
	
		
	public static void main(String[] args) throws FileNotFoundException, IOException{
		
		
		PropertyMap prop = PropertyLoader.loadProperties();
	
		String portValue = prop.get("server.port").orElse("8080");
/*
		String mongoPort = prop.get("mongo.port").orElse("27017");
		String mongoHost = prop.get("mongo.host").orElse("localhost");
*/
		String mongoUser = prop.get("mongo.user").orElse("");
		String mongoPassword = prop.get("mongo.password").orElse("");

		//String mongoPort = prop.get("mongo.port").orElse("27017");
		//String mongoHost = prop.get("mongo.host").orElse("localhost");
		String mongoPort = prop.get("verbs.db.service.service.port").orElse("27017");
		String mongoHost = prop.get("verbs.db.service.service.host").orElse("localhost");
		
		String mongoDb = prop.get("mongo.db").orElse("verbs");
		String mongoRemote = prop.get("mongo.remote").orElse("false");
		
		
		log.info("*********");
		prop.entrySet().forEach(entry -> log.info(entry.getKey() + ": " + entry.getValue()));
		

		/*mongoRemote="true";
		mongoUser="spanish-reader";
		mongoPassword="EoSwYTeMP0iQ7aPr";
		mongoHost="cluster0-skws2.mongodb.net";*/

		log.info("Running on port: " + portValue);
		log.info("Db Port: " + mongoPort);
		log.info("Mongo Host: " + mongoHost);
		log.info("Mongo Db: " + mongoDb);
		
		
		
		MongoClient mongoClient;
		if(Boolean.parseBoolean(mongoRemote)) {
			
			mongoClient = createRemoteClient(mongoUser, mongoPassword, mongoHost, mongoDb);
		} else {
			mongoClient = createLocalClient(mongoHost, Integer.parseInt(mongoPort));
		}
		
		MongoDatabase database = mongoClient.getDatabase(mongoDb);
		
		DocumentAdapter adapter = new DocumentAdapter();
		
		VerbRepository verbRepo = new VerbRepository(database, adapter);
		
		VerbService service = new VerbService(verbRepo);
		
		Javalin app = Javalin.create(config -> {
			config.requestLogger((ctx, ms) -> {
			    log.info(ctx.method() + " "  + ctx.path() + " took " + ms + " ms");
		    });
			config.registerPlugin(new RouteOverviewPlugin("/"));
			config.defaultContentType = APPLICATION_JSON_UTF8;
		});
		
		
		app.get("/healthz", ctx -> {
			log.info("Health Check");
			ctx.status(200);
			ctx.result("OK");
		});

		app.after(ctx -> new CorsHandler());
		
		VerbController controller = new VerbController(app, verbRepo, service);
		controller.setup();		
		app.start(Integer.parseInt(portValue));
		
		
		verbRepo.findRegulars().forEach(System.out::println);
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
				mongoClient.close();
				
			}
		}));
	}
}


